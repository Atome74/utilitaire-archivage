#!/bin/bash

pip install smbprotocol
pip install requests

line="0 2 * * * python3 $PWD/archiveur.py"
(crontab -u $(whoami) -l; echo "$line" ) | crontab -u $(whoami) -

python3 $PWD/archiveur.py

echo "Veuillez renseigner les différentes informations de configuration dans le fichier 'configuration.ini'"
