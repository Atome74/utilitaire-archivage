#!/usr/bin/env python
# -*- coding:utf-8 -*-

import configparser
import datetime
import hashlib
import os
import smtplib
import tarfile
import traceback
import urllib.request
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from enum import Enum
from zipfile import ZipFile

import requests
from smbclient import *
from smbprotocol.header import *
from smbprotocol.structure import *

# Configuration interne

PATH_CONFIG_INI = 'configuration.ini'
PATH_SVG_MD5 = '.md5.sauvegarde'
NAME_TEMP = 'temp'
PATH_TEMP_LOG = '.temp_log'
LOG_FILE = "archivage_log.txt"

# Variables globales

config = configparser.ConfigParser()
tableauReport = {}  # tableau stockant les états de succès de chaque opération

# Classes Enum pour la gestion des erreurs


class Etapes(Enum):
    CONFIG_FILE_PARSED = 1
    ZIP_FILE_DOWNLOADED = 3
    ZIP_FILE_CONTAINS_SQL = 4
    SQL_IS_NOT_THE_SAME = 5
    TGZ_CREATED = 6
    UPLOAD_SERVER_PATH_EXISTS = 7
    UPLOAD_WORKED = 8
    HISTORY_CLEANED = 9


class Etats(Enum):
    OK = 1
    WARNING = 2
    ERROR = 3

    def __ge__(self, other):
        if self.__class__ is other.__class__:
            return self.value >= other.value
        return NotImplemented

    def __gt__(self, other):
        if self.__class__ is other.__class__:
            return self.value > other.value
        return NotImplemented

    def __le__(self, other):
        if self.__class__ is other.__class__:
            return self.value <= other.value
        return NotImplemented

    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.value < other.value
        return NotImplemented


# Fonctions
# Utilitaires

def log(message, erreur=False, exception=None, debug=True):
    message = str(message)
    if erreur:
        if (exception != None):
            if (debug):
                print(traceback.format_exc())
            if (message != ""):
                message += " - "
            message += str(exception)
        message = "Erreur : " + message
    message = str(datetime.datetime.now()) + " > " + message
    
    with open(LOG_FILE, 'a') as fichier:
        fichier.write(message + "\n")
    with open(PATH_TEMP_LOG, 'a') as fichier:
        fichier.write(message + "\n")
    print(message)


def removeFileIfExists(filename):
    if os.path.exists(filename):
        try:
            os.remove(filename)
        except:
            True
        True

# Fonctionnalitees


def loadConfiguration():
    tableauReport[Etapes.CONFIG_FILE_PARSED] = Etats.ERROR
    try:
        if not os.path.exists(PATH_CONFIG_INI):
            config['GENERAL'] = {
                'url_serveur_source': 'https://<source>.zip',
                'path_dossier_serveur_destination': r"\\server\share",
                'username': "username",
                'password': "password"
            }

            config['HISTORY'] = {'number_days_kept': '7'}

            config['EMAIL'] = {
                'notification_active': "always",
                'smtp_server': "smtp.gmail.com",
                'stmp_port': 587,
                'sender_username': "example@gmail.com",
                'sender_password': "password",
                'sender_email': "example@gmail.com",
                'receivers': "example@gmail.com, athome74@free.fr",
                'email_object_ok': "OK Rapport d'archivage",
                'email_object_warning': "WARNING Rapport d'archivage ",
                'email_object_error': "ERROR Rapport d'archivage",
                'add_attachment': "enabled"}

            config['MATTERMOST'] = {
                'notification_active': "always",
                'url_webhook': r'https://example.fr/hooks/<key>'}

            config['INTERNAL'] = {'log_path': "archivage_log.txt"}

            LOG_FILE = config['INTERNAL']['log_path']

            with open(PATH_CONFIG_INI, 'w') as configfile:
                configfile.write(" ")
                config.write(configfile)
                log("Creation d'un nouveau fichier '" + PATH_CONFIG_INI + "'")
            return
        else:
            tableauReport[Etapes.CONFIG_FILE_PARSED] = Etats.OK
        config.read(PATH_CONFIG_INI)

        connexionServeur()
    except Exception as exp:
        log("Chargement du fichier '" + PATH_CONFIG_INI +
            "' a hauteur de '" + exp.args[0] + "'", True, exp)
        


def connexionServeur():
    tableauReport[Etapes.UPLOAD_SERVER_PATH_EXISTS] = Etats.ERROR
    try:
        serverAddress = config['GENERAL']['path_dossier_serveur_destination'].split("\\", 3)[
            2]
        register_session(
            serverAddress, username=config['GENERAL']['username'], password=config['GENERAL']['password'])
        tableauReport[Etapes.UPLOAD_SERVER_PATH_EXISTS] = Etats.OK
        try:
            download()
        except Exception as exp:
            log("Phase execution routine nouveau fichier", True, exp)
        try:
            controleArchives()
        except Exception as exp:
            log("Phase suppression", True, exp)
    except ValueError as exp:
        log("Serveur destination '" + serverAddress + "' inaccessible", True, exp)

    except TypeError as exp:
        try:
            if (exp.args[0]['status'].value == NtStatus.STATUS_LOGON_FAILURE):
                log("Authentification incorrecte au serveur destination '" +
                    serverAddress + "'. Verifier username/password", True)
                return

            log("Connexion au serveur impossible " +
                str(exp.args[0]['status']), True, exp)
        except:
            raise exp
    except Exception as exp:
        log("Connexion au serveur destination impossible", True, exp)


def download():
    tableauReport[Etapes.ZIP_FILE_DOWNLOADED] = Etats.ERROR
    try:
        removeFileIfExists(NAME_TEMP + ".zip")
        urllib.request.urlretrieve(config['GENERAL']['url_serveur_source'], "{0}".format(
            NAME_TEMP + ".zip"))
        log("Le fichier '" + NAME_TEMP + ".zip" + "' a ete telecharge")
        tableauReport[Etapes.ZIP_FILE_DOWNLOADED] = Etats.OK
        extract()
        removeFileIfExists(NAME_TEMP + ".zip")

    except Exception as exp:
        if (str(exp) == "HTTP Error 404: Not Found"):
            log("Le zip n'est pas accessible", True, exp)
            return
        log("Le zip n'a pas pu etre telecharge", True, exp)


def extract():
    tableauReport[Etapes.ZIP_FILE_CONTAINS_SQL] = Etats.ERROR
    try:
        with ZipFile(NAME_TEMP + ".zip", 'r') as obj_zip:
            for filename in obj_zip.namelist():
                if filename.endswith('.sql'):
                    removeFileIfExists(filename)
                    obj_zip.extract(filename)
                    log("Le fichier '" + filename + "' a ete extrait")
                    tableauReport[Etapes.ZIP_FILE_CONTAINS_SQL] = Etats.OK
                    checkIsNotTheSameFile(filename)
                    removeFileIfExists(filename)
                    return
            log("Aucun fichier avec l'extension .sql n'a pas ete trouve dans l'archive telechargee", True)
    except Exception as exp:
        log("Extraction du fichier", True, exp)


def getMd5():
    if os.path.exists(PATH_SVG_MD5):
        with open(PATH_SVG_MD5, 'r') as fichier:
            return fichier.read()
    return ""


def setMd5(hash):
    with open(PATH_SVG_MD5, 'w') as fichier:
        fichier.write(hash)


def sumfile(filePath):
    fileObj = open(filePath, 'rb')
    m = hashlib.md5()
    while True:
        d = fileObj.read(8096)
        if not d:
            break
        m.update(d)
    return m.hexdigest()


def checkIsNotTheSameFile(filename):
    tableauReport[Etapes.SQL_IS_NOT_THE_SAME] = Etats.ERROR
    try:
        md5 = sumfile(filename)
        if getMd5() != md5:
            setMd5(md5)
            log("Le fichier est bien different de celui de la veille")
            tableauReport[Etapes.SQL_IS_NOT_THE_SAME] = Etats.OK
            compress(filename)
        else:
            log("Le fichier est identique", True)
    except Exception as exp:
        log("Verification fichier equivalent veille", True, exp)


def compress(filename):
    tableauReport[Etapes.TGZ_CREATED] = Etats.ERROR
    try:
        tgzFileName = datetime.datetime.now().strftime(r'%Y%m%d.tgz')
        removeFileIfExists(tgzFileName)
        with tarfile.open(tgzFileName, "w:gz") as tar:
            tar.add(filename)
        tableauReport[Etapes.TGZ_CREATED] = Etats.OK
        sendFile(tgzFileName)
        removeFileIfExists(tgzFileName)
    except Exception as exp:
        log("Compression du fichier", True, exp)


def sendFile(tgzFileName):
    tableauReport[Etapes.UPLOAD_WORKED] = Etats.ERROR
    try:
        try:
            with open_file(config['GENERAL']['path_dossier_serveur_destination'] + "\\" + tgzFileName, mode="wb") as fd:
                with open(tgzFileName, "rb") as f:
                    fd.write(f.read())
            log("Le fichier '"+tgzFileName+"' a ete transfere sur le serveur SMB '" +
                config['GENERAL']['path_dossier_serveur_destination'] + "'")
            tableauReport[Etapes.UPLOAD_WORKED] = Etats.OK

        except FileNotFoundError as exp:
            log("Interne : le fichier '" + tgzFileName + "' est introuvable")
        except Exception as exp:
            if (exp.ntstatus == NtStatus.STATUS_ACCESS_DENIED):
                log("Acces en ecriture a '" + config['GENERAL']['path_dossier_serveur_destination'] +
                    "\\" + tgzFileName + "' refuse", True)
                return
            raise exp
    except Exception as exp:
        log("Envoi du fichier sur serveur destination", True, exp)


def controleArchives():
    tableauReport[Etapes.HISTORY_CLEANED] = Etats.WARNING
    today = datetime.date.today()
    try:
        for file_info in scandir(config['GENERAL']['path_dossier_serveur_destination']):
            file_inode = file_info.inode()
            if (file_info.is_file() and file_info.name.endswith('.tgz')):
                try:
                    tempsFile = datetime.datetime.strptime(
                        file_info.name, r'%Y%m%d.tgz').date()
                except:
                    True
                else:
                    if (int((today - tempsFile).days) > int(config['HISTORY']['number_days_kept'])):
                        supprimerFichier(
                            file_info.name, (today - tempsFile).days)
                        tableauReport[Etapes.HISTORY_CLEANED] = Etats.WARNING

    except Exception as exp:
        try:
            if (exp.ntstatus == NtStatus.STATUS_ACCESS_DENIED):
                log("Acces en lecture a '" +
                    config['GENERAL']['path_dossier_serveur_destination'] + "' refuse", True)
                return
            raise exp
        except Exception as expe:
            log("Controle des archives", True, exp)


def supprimerFichier(file_info_name, tempsFile):
    try:
        remove(
            config['GENERAL']['path_dossier_serveur_destination'] + "\\" + file_info_name)
        log("Suppression du fichier '" + file_info_name +
            "' d'il y a " + str(tempsFile) + " jours")
        tableauReport[Etapes.HISTORY_CLEANED] = Etats.OK
    except Exception as exp:
        try:
            if (exp.ntstatus == NtStatus.STATUS_ACCESS_DENIED):
                log("Acces en ecriture a '" + config['GENERAL']['path_dossier_serveur_destination'] +
                    "\\" + file_info_name + "' refuse", True)
                return
            log("Suppression sauvegarde", True, exp)
        except Exception as exp:
            log("Suppression sauvegarde", True, exp)


def report():
    messages = {
        Etapes.CONFIG_FILE_PARSED:        "Chargement du fichier de config ",
        Etapes.ZIP_FILE_DOWNLOADED:       "Téléchargement du fichier       ",
        Etapes.ZIP_FILE_CONTAINS_SQL:     "Dump SQL présent                ",
        Etapes.SQL_IS_NOT_THE_SAME:       "Comparaison des fichiers        ",
        Etapes.TGZ_CREATED:               "Compression du fichier          ",
        Etapes.UPLOAD_SERVER_PATH_EXISTS: "Connexion au serveur SMB        ",
        Etapes.UPLOAD_WORKED:             "Fichier recu sur le serveur     ",
        Etapes.HISTORY_CLEANED:           "Anciennes sauvegardes purgées   "
    }

    pictos = {
        Etats.OK:      ":white_check_mark:",
        Etats.WARNING: ":warning:         ",
        Etats.ERROR:   ":x:               "
    }

    etatTotal = max(tableauReport.values())

    envoi = "# Rapport exécution script\n"
    envoi += str(datetime.datetime.now()) + "\n"

    if (etatTotal == Etats.OK):
        envoi += "### :white_check_mark: Aucune erreur détectée\n"
    elif (etatTotal == Etats.WARNING):
        envoi += "### :warning: Des alertes ont été détectées\n"
    else:
        envoi += "### :x: Des erreurs ont été détectées\n"

    envoi += "|  Étape                           |  État              |\n"
    envoi += "|:---------------------------------|:------------------:|\n"

    for etape in Etapes:
        try:
            envoi += "| " + messages[etape] + " | " + \
                pictos[tableauReport[etape]] + " |\n"
        except KeyError:
            True

    print("\n" + envoi)
    try:
        sendMattermostReport(envoi, etatTotal)
    except Exception as exp:
        log("Envoi notification Mattermost", True, exp)
    try:
        sendMailReport(envoi, etatTotal)
    except Exception as exp:
        log("Envoi notification Email", True, exp)


def sendMailReport(envoi, etatTotal):
    try:
        if (config['EMAIL']['notification_active'] == 'always' or (config['EMAIL']['notification_active'] == 'warning' and etatTotal >= Etats.WARNING) or (config['EMAIL']['notification_active'] == 'error' and etatTotal == Etats.ERROR)):

            for destinataire in config['EMAIL']['receivers'].split(","):

                Fromadd = config['EMAIL']['sender_username']

                message = MIMEMultipart()
                message['From'] = config['EMAIL']['sender_email']
                message['To'] = destinataire.replace(" ", "")
                if (etatTotal == Etats.OK):
                    message['Subject'] = config['EMAIL']['email_object_ok']
                elif (etatTotal == Etats.WARNING):
                    message['Subject'] = config['EMAIL']['email_object_warning']
                else:
                    message['Subject'] = config['EMAIL']['email_object_error']
                msg = envoi
                message.attach(MIMEText(msg.encode('utf-8'), 'plain', 'utf-8'))

                if (config['EMAIL']['add_attachment'] == "enabled"):
                    nom_fichier = datetime.datetime.now().strftime(r'%Y-%m-%d.txt')
                    piece = open(PATH_TEMP_LOG, "rb")
                    part = MIMEBase('application', 'octet-stream')
                    part.set_payload((piece).read())
                    encoders.encode_base64(part)
                    part.add_header('Content-Disposition',
                                    "piece; filename= %s" % nom_fichier)
                    message.attach(part)

                serveur = smtplib.SMTP(
                    config['EMAIL']['smtp_server'], config['EMAIL']['stmp_port'])
                serveur.starttls()
                serveur.login(Fromadd, config['EMAIL']['sender_password'])
                texte = message.as_string().encode('utf-8')
                serveur.sendmail(Fromadd, destinataire.replace(" ", ""), texte)
                serveur.quit()

                log("Email envoye a '" + destinataire.replace(" ", "") + "'")
    except Exception as exp:
        log("Envoi email", True, exp)

def sendMattermostReport(msg, etatTotal):
    try:
        if (config['MATTERMOST']['notification_active'] == 'always' or (config['MATTERMOST']['notification_active'] == 'warning' and etatTotal >= Etats.WARNING) or (config['MATTERMOST']['notification_active'] == 'error' and etatTotal == Etats.ERROR)):

            headers = {'Content-Type': 'application/json', }
            envoi = '{"text": "\n'
            envoi += msg
            envoi += '"}'
            response = requests.post(
                config['MATTERMOST']['url_webhook'], headers=headers, data=envoi.encode("utf-8"))
            log("Msg Mattermost envoye")
    except Exception as exp:
        log("Envoi notification Mattermost", True, exp)


if __name__ == "__main__":
    log("Debut du script")

    loadConfiguration()  # Debut de l'appel recursif des sucessives fonctions d'archivage

    log("Fin de la sauvegarde")

    report()             # Envoi des notifications du deroulement de l'archivage

    log("Fin du script")

    removeFileIfExists(PATH_TEMP_LOG)
