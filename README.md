# Projet Scripting System

## Documentation utilisateur

Auteur Alban THOMÉ

# Script d'archivage distant

## Fonctionnalitées :

Ce script permet de télécharger un fichier zip depuis une adresse web, d'en extraire le contenu, rechercher un fichier sql, le comparer à celui de la veille, le compresser en tgz, de l'envoyer sur un serveur SMB distant.

De plus, il permet une surveillance de son bon déroulement à l'aide de notifications email et Mattermost configurables



## Mise en route :

À l'intérieur du dossier du projet, exécuter le bash d'installation

```shell
./setup.sh
```



## Configuration :

Le fichier de configuration se nomme ```configuration.ini``` et se trouve dans le dossier du projet.

Le cas échéant, il est créé par le script avec des valeurs par défaut



### Variables configurables :

| Catégorie  | Nom du champ                     | Description                                                  |
| :--------: | :------------------------------- | :----------------------------------------------------------- |
|  GENERAL   | url_serveur_source               | Url du serveur source où se trouve le .zip                   |
|  GENERAL   | path_dossier_serveur_destination | Path jusqu'au dossier de sauvegarde sur le serveur de destination |
|  GENERAL   | username                         | Identifiant du serveur SMB                                   |
|  GENERAL   | username                         | Mot de passe du serveur SMB                                  |
|  HISTORY   | number_days_kept                 | Nombre de jours de conservation des archives sur le serveur de destination |
|   EMAIL    | notification_active              | [ always \| warning \| error \| never ] : Active les notifications par mail |
|   EMAIL    | smtp_server                      | adresse du serveur smtp                                      |
|   EMAIL    | stmp_port                        | port du serveur smtp                                         |
|   EMAIL    | sender_username                  | identifiant de l'utilisateur sur le serveur smtp             |
|   EMAIL    | sender_password                  | mot de passe de l'utilisateur sur le serveur smtp            |
|   EMAIL    | sender_email                     | email affiché de l'utilisateur                               |
|   EMAIL    | receivers                        | liste des emails, séparés par une virgule, qui recevront les notifications |
|   EMAIL    | email_object_ok                  | Objet de l'email dans le cas nominal                         |
|   EMAIL    | email_object_warning             | Objet de l'email en cas d'avertissements                     |
|   EMAIL    | email_object_error               | Objet de l'email en cas d'erreur bloquante                   |
|   EMAIL    | add_attachment                   | [ enabled \| disabled ] ajoute  en pièce jointe le rapport de log brut |
| MATTERMOST | notification_active              | [ always \| warning \| error \| never ] : Active les notifications par mail |
| MATTERMOST | url_webhook                      | l'url du webhook pour l'envoi des messages Mattermost        |



## Dépendances :

Le script d'archivage nécessite les packages suivants :

```shell
pip install smbprotocol
pip install requests
```

