# Projet Scripting System

## Mémoire technique

Auteur Alban THOMÉ

# Script d'archivage distant



## Architecture :

Ce projet est articulé autour d'un fichier de script python ```archivage.py``` comprenant les différentes fonctionnalités.

Ces dernières sont atomisées en fonctions, suivant un ordre d'appel en profondeur

De cette façon, on s'assure de n'executer l'étape suivante de la procédure qu'en cas de succès de la précédente. En effet, cette caractéristique est inhérente au cahier des charges : Une fonction est dépendante des précédentes dans l'ordre d'appel pour pouvoir être exécutée.

Cela permet aussi d'effacer aisément les fichiers temporaires créés par chaque fonction



## Fonctions et choix de packages :

1) Chargement de la configuration

   Le format retenu est celui .ini, standard largement répendu et simple. Il est utilisé dans le script via le package ```configparser```

2) Connexion au serveur destination

   Cette étape étant bloquante par la suite et partagée entre plusieurs fonctions, elle est effectuée dès le début du script

   Le package externe ```smbprotocol``` est utilisé pour la connexion SMB à l'aide des identifiants du fichier de configuration

   Une session de connexion est instanciée pour toute la durée de l'exécution du script

   

3) Téléchargement du fichier .zip

   À l'aide du package ```urllib.request```

4) Extraction du fichier .zip

   Recherche d'un fichier comportant l'extension .sql

   Le premier rencontré sera celui sauvegardé

5) Vérification du dump .sql

   Effectué à l'aide d'une checksum md5 du package ```hashlib```, stockée dans un fichier caché, ce qui permet de comparer les fichiers d'une manière satisfaisante à un bas coût mémoire

6) Compression en .tgz

   À l'aide du package ```tarfile```. Le nom du fichier est au format ```AAAADDMM.tgz``` 

7) Envoi du fichier sur le serveur de destination

   La connexion est déjà établie, l'envoi s'effectue aussi via ```smbprotocol```

   

8) Contrôle des archives présentes sur le serveur distant

   Analyse du dossier de destination, recherche des fichiers respectant le format ```AAAADDMM.tgz```.

   Recherche des fichiers dont la date (provenant du nom du fichier) précède la date courante moins le nombre de jours de conservation, fixé dans le fichier configuration.ini

9) Suppression de chacun de ces fichiers

   

10) Création du rapport

    Au fur et à mesure de l'appel récursif de chaque fonction, le tableau global ```tableauReport``` est rempli à l'aide de deux classes énumérées représantant les différentes étapes et états par lequel passe le script. 

    Celui-ci est alors transformé en tableau au format markdown

11) Envoi des Emails

    Suivant les préférences renseignées dans le fichier de configuration - voir [doc utilisateur](README.md)

    Fonctionne via les packages ```email.mime``` pour la construction du message et ```smtplib``` pour l'envoi

12) Envoi de la notification Mattermost

    Suivant les préférences renseignées dans le fichier de configuration - voir [doc utilisateur](README.md)

    Fonctionne via le package externe ```requests``` via une requete POST vers l'addresse webhook renseignée

    

## Exécution :

Le script est exécuté périodiquement à l'aide de la fonctionnalité Linux ```cron``` 

Une nouvelle entrée est ajouté à la ```crontab``` lors de l'installation du script, dans le fichier ```setup.sh``` Celle-ci lance le script une fois par jour, à 02:00, afin de sauvegarder à un moment où la charge serveur est supposée minimale



## Exceptions :

De nombreuses exceptions sont levées par les différents packages, et attrapés pour être consignées dans la fonction de log, qui remplit un fichier de log dont l'emplacement est configurable.

Dans un objectif de stabilité, l'ensemble des opérations de sauvegarde et de notification sont séparées et cette dernière protégée, de sorte qu'une erreur entraine tout de même l'envoi du rapport d'alerte
